import argparse
import os
import threading
import time
from queue import Queue
import grpc
from concurrent import futures
import rd_le_pb2
import rd_le_pb2_grpc
from signal import SIGTERM


class ElectionMessage:
    def __init__(self, senderPort, receiverPort, senderID, counter):
        self.senderPort = senderPort
        self.receiverPort = receiverPort
        self.senderID = senderID
        self.counter = counter


class ReplyMessage:
    def __init__(self, senderPort, receiverPort, reply, overlap, counter):
        self.senderPort = senderPort
        self.receiverPort = receiverPort
        self.reply = reply
        self.overlap = overlap
        self.counter = counter


class LeaderElectedMessage:
    def __init__(self, senderPort, leaderID):
        self.senderPort = senderPort
        self.leaderID = leaderID


class IterationFinishedMessage:
    def __init__(self, senderPort):
        self.senderPort = senderPort


class RDLeaderElectionServicer(rd_le_pb2_grpc.RDLeaderElectionServicer):
    def __init__(self, node):
        super(RDLeaderElectionServicer, self).__init__()
        self.node = node
        self.m_count = 0

    def SendEM(self, request, context):
        port = list(self.node.out_ports)
        port.remove(request.senderPort)

        if request.senderID not in self.node.received:
            overlap = False
        else:
            overlap = True

        if request.counter - 1 == 0:
            if request.senderID < self.node.nodeID:
                reply = 'YES'
                self.node.defeated = True
            else:
                reply = 'NO'

            self.node.RM_queue.put(ReplyMessage(self.node.port, request.senderPort, reply, overlap, 2 ** (self.node.iteration_counter - 1)))
        else:
            if request.senderID > self.node.nodeID:
                reply = 'NO'
                self.node.RM_queue.put(ReplyMessage(self.node.port, request.senderPort, reply, overlap, 2 ** (self.node.iteration_counter - request.counter - 2)))
            else:
                self.node.defeated = True
                self.node.EM_queue.put(ElectionMessage(self.node.port, port[0], request.senderID, request.counter - 1))

        self.node.received.append(request.senderID)
        return rd_le_pb2.Empty()

    def Reply(self, request, context):
        port = list(self.node.out_ports)
        port.remove(request.senderPort)
        if request.receiverPort == 50285:
            with open('node.txt', 'a') as file:
                file.write('Received RM: from {0} to {1} in iteration {2} (counter {3}) {4}\n'.format(request.senderPort - 50280,
                                                                                         request.receiverPort - 50280,
                                                                                         self.node.iteration_counter,
                                                                                         request.counter, request.reply))
        if request.counter - 1 == 0:
            self.node.received_rm += 1
            if request.reply == 'YES':
                self.node.received_yes += 1

            if self.node.received_rm == 2 and self.node.received_yes != 2:
                self.node.defeated = True

            if request.reply == 'YES' and request.overlap == True:
                self.node.leaderID = self.node.nodeID
                self.node.sendLeaderElected(self.node.leaderID, self.node.out_ports[0])
                self.node.sendLeaderElected(self.node.leaderID, self.node.out_ports[0])
                self.node.leader_elected = True

        else:
            self.node.RM_queue.put(ReplyMessage(self.node.port, port[0], request.reply, request.overlap, request.counter - 1))

        if self.node.received_rm == 2:
            self.node.finished = True

        return rd_le_pb2.Empty()

    def LeaderElected(self, request, context):
        self.node.leaderID = request.leaderID
        port = list(self.node.out_ports)
        port.remove(request.senderPort)
        self.node.sendLeaderElected(request.leaderID, port[0])
        self.node.leader_elected = True
        os.kill(os.getppid(), SIGTERM)
        return rd_le_pb2.Empty()

    def IterationFinished(self):
        self.node.iteration_finished_counter += 1
        return rd_le_pb2.Empty()


class Node:
    def __init__(self, nodeID, port, neighbours):
        self.nodeID = nodeID
        self.port = port
        self.neighbours = neighbours
        self.iteration_counter = 1
        self.mutex = threading.Lock()

        self.EM_queue = Queue()
        self.RM_queue = Queue()
        self.out_ports = (neighbours[0]["port"], neighbours[1]["port"])

        self.servicer = RDLeaderElectionServicer(self)

        self.defeated = False
        self.received = []
        self.received_yes = 0
        self.received_rm = 0
        self.finished = False
        self.leader_elected = False
        self.leaderID = None
        self.iteration_finished_counter = 0

    def sendElectionMessage(self, message):
        if message.senderPort == 50285:
            with open('node.txt', 'a') as file:
                file.write('EM: from {0} to {1} in iteration {2} (counter {3})\n'.format(message.senderPort-50280, message.receiverPort-50280, self.iteration_counter, message.counter))

        with grpc.insecure_channel('localhost:' + str(message.receiverPort)) as channel:
            stub = rd_le_pb2_grpc.RDLeaderElectionStub(channel)
            stub.SendEM(rd_le_pb2.EM(senderPort=message.senderPort, receiverPort=message.receiverPort, senderID=message.senderID, counter=message.counter))

    def sendReply(self, message):
        if message.senderPort == 50285:
            with open('node.txt', 'a') as file:
                file.write('RM: from {0} to {1} in iteration {2} (counter {3}) {4}\n'.format(message.senderPort-50280, message.receiverPort-50280, self.iteration_counter, message.counter, message.reply))

        with grpc.insecure_channel('localhost:' + str(message.receiverPort)) as channel:
            stub = rd_le_pb2_grpc.RDLeaderElectionStub(channel)
            a = rd_le_pb2.RM(senderPort=message.senderPort, receiverPort=message.receiverPort, reply=message.reply, overlap=message.overlap, counter=message.counter)
            stub.Reply(a)

    def sendLeaderElected(self, leaderID, port):
        with open('node.txt', 'a') as file:
            file.write('Leader Elected {0} (I am node {1}) \n'.format(leaderID, self.nodeID))

        with grpc.insecure_channel('localhost:' + str(port)) as channel:
            stub = rd_le_pb2_grpc.RDLeaderElectionStub(channel)
            stub.LeaderElected(rd_le_pb2.LEM(senderPort=self.port, leaderID=leaderID))

    def sendIterationFinished(self, port):
        with grpc.insecure_channel('localhost:' + str(port)) as channel:
            stub = rd_le_pb2_grpc.RDLeaderElectionStub(channel)
            a = rd_le_pb2.IFM(senderPort=self.port)
            stub.IterationFinished(a)

    def serve(self):
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        rd_le_pb2_grpc.add_RDLeaderElectionServicer_to_server(self.servicer, server)
        server.add_insecure_port('[::]:'+str(self.port))
        server.start()

        while not self.leader_elected:
            if self.nodeID == 5:
                with open('node.txt', 'a') as file:
                    file.write(
                        'Start Iteration {0}: node is defeated {1}\n'.format(self.iteration_counter, self.defeated))

            if not self.defeated:
                self.sendElectionMessage(ElectionMessage(self.port, self.out_ports[0], self.nodeID, 2 ** (self.iteration_counter - 1)))
                self.sendElectionMessage(ElectionMessage(self.port, self.out_ports[1], self.nodeID, 2 ** (self.iteration_counter - 1)))

            while not self.finished:
                while not(self.EM_queue.empty()):
                    self.sendElectionMessage(self.EM_queue.get())
                    time.sleep(0.1)

                while not(self.RM_queue.empty()):
                    self.sendReply(self.RM_queue.get())
                    time.sleep(0.1)

            self.sendIterationFinished(self.out_ports[0])
            self.sendIterationFinished(self.out_ports[1])

            while self.iteration_finished_counter != 2:
                while not(self.EM_queue.empty()):
                    self.sendElectionMessage(self.EM_queue.get())
                    time.sleep(0.1)

                while not(self.RM_queue.empty()):
                    self.sendReply(self.RM_queue.get())
                    time.sleep(0.1)

            self.iteration_counter += 1
            self.received = []
            self.received_rm = 0
            self.received_yes = 0
            self.finished = False
            self.iteration_finished_counter = 0

        server.stop(None)
        exit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Node creation for Recursive Doubling Leader Election method.')
    parser.add_argument("-i", "--id", required=True, type=int, help="id of the current node")
    parser.add_argument("-p", "--port", required=True, type=int, help="port of the current node server")
    parser.add_argument("-n", "--neighbours", nargs=4, action='append', required=True, type=int,
                        help="id and port of the neighbours of the current node")
    parser.add_argument("-s", "--start", action=argparse.BooleanOptionalAction, default=False,
                        help="starting node of the Election procedure")

    args = vars(parser.parse_args())

    # Setup of parsed argument
    id = args['id']
    port = args['port']
    neighbours = []
    print(args['neighbours'])

    neighbours.append({"id": args['neighbours'][0][0], "port": args['neighbours'][0][1]})
    neighbours.append({"id": args['neighbours'][0][2], "port": args['neighbours'][0][3]})

    # Global useful parameters
    global start
    start = args['start']

    # Node instanciation
    Node = Node(id, port, neighbours)

    # Service launching
    Node.serve()






