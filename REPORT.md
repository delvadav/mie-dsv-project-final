**MIE-DSV-Project**

**Project description**


The goal of this project is to propose an implementation of the Recursive Doubling algorithm for Leader Election in a distributed system using Google RPC(grpc) technology.


**How to use ?**

To run the program you will only need to use the manager.py program. You can use it in two different ways:

First with a direct topology specified in the command line arguments using -t option.

python3 manager.py -t TOPOLOGY
The TOPOLOGY here is specified using comas like shown:
node1,node2,node3,...,nodeN
For example:
python3 manager.py -t 1,2,4,7

The second way of using the program is by giving a file containing several topologies in the command line arguments using -f option.

python3 manager.py -f FILENAME
The topologies in the file needs to be specified the same way as for the direct option, each line corresponding to a single topology.


**Architecture description**

The architecture of the project is based on different main parts:

The grpc working files: rd_le.proto, rd_le_pb2.py, rd_le_pb2_grpc.py

The node file running a single node: node.py

The main program creating the topology and running the nodes: manager.py


**Recursive Doubling Leader Election algorithm**

The algorithm implemented for this solution is the Recursive Doubling algorithm for leader election in a distributed system using a ring topology.
This algorithm consists on different phases controlled by Iteration Finished Messages (IFM) of Election Messages(EM) and Reply Messages(RM)  to elect the node that has the minimum ID as a leader via LeaderElectedMessage(LEM).
To run this algorithm every node need to send two EM to each of his nodes into a distance determined by its phase (2^k-1, being k the phase that always starts in 1). 
The phase will be finished when the node receives its two corresponding Reply Messages(RM) back.
The algorithm will finish when a node recieves one YES response in his RM and an overlap, indicating he´s the only node alive in the topology.


**GRPC implementation**

To implement the node to node communication we had to use the grpc framework.

Definition of the service
The first step was to define the service to communicate from a node to another. This definition is performed in the rd_le.proto file. The service is called RDLeaderElection and is composed of four functions: SendEM, Reply, LeaderElected and IterationFinished. Each of them uses their own kind of message defined below.
All function returns Empty message because we don't use the return part of a grpc service here.

The EM messages are defined with these parameters:

senderPort: corresponding to the port of the neighbour node that transferred the message
receiverPort: corresponding to port of the receiver ID 
senderID: corresponding to the sender ID node
counter: used to count the number of node in the topology

The RM messages are defined with these parameters:

senderPort: corresponding to the port of the neighbour node that transferred the message
receiverPort: corresponding to port of the receiver ID 
reply: string corresponding to the response of the last node that received the EM
overlap: boolean that indicates if there exists overlaping
counter: used to count the number of node in the topology

The LEM messages are defined with these parameters:

senderPort: corresponding to the port of the neighbour node that transferred the message
leaderID: contains ID of the leader elected

The IFM messages are defined with these parameters:


This .proto file allows us to automatically generate rd_le_pb2.py and rd_le_pb2_grpc.py files which contains useful classes and function to use to implement the service.

**Node**

The core of the architecture is stored in the node.py file. This file correspond to the code for a single node.
This file is composed of several classes:

RDLeaderElectionServicer: implementing the Service defined in the proto file
Node: the main class for a single node containing all the information and implementation of the node
ElectionMessage: allows to store EM
ReplyMessage: allows to store RM
LeaderElectedMessage: allows to store LEM
IterationFinishedMessage: allows to store IFM


**Node class**

This class is representing a single node with all its useful attributes. It contains its id, the list of its neighbours, its port of communication, the number of node in the ring (to be computed) and the elected leader(to be computed as well) along with other functional attributes.
There are 3 functions to this class:

sendElectionMessage: used to send EM to other nodes by specifying their port
sendReply: used to send RM to other nodes by specifying their port
sendLeaderElected: used to send lEM to other nodes by specifying their port
sendIterationFinished: used to send IFM to other nodes by specifying their port
serve: This function is used to start the server to listen to the port of the node and it is also use to send message in reaction to received messages.

The messages to send are defined by the servicer. When a node receive a message the servicer performs the RD algorithm and add some messages to a queue to notify the node class that it needs to send a message.
In the serve function the queues are checked and de-queued in order to send the messages while the node is running. The way our algorithm is implemented, we need both queues to dequeue at the same time.
When a node doesn´t receive two RM saying 'YES', the node defeats itself and cannot

**RDLeaderElectionServicer class**

This class is a child of a auto generated class of the grpc framework implementing the service defined in the proto file. It contains 2 functions corresponding to the service functions:

SendEM: For receiving EM and to perform the algorithm
Reply: For handling RM  to terminate the node
LeaderElected: For sending LEM and terminating the program
IterationFinished: For sending IFM 

This class is linked to the Node object of the program. The Node holds an object of the servicer and this servicer hold a reference to its attached node. This is how the servicer and the node communicate.

**SendEM**

First it checks if it overlaps. 
Then forwards the message if it doesn´t have lesser ID than the sender. Otherwise the message is returned via RM queue with a NO reply.
If the message is finding himself at the end of its range, the message isn´t more forwarded and just responds with ÝES or NO and defeates the node with the same criteria as above.

**Reply**

Checks the Reply Messages recieved by the node, defeating them if they don´t have two YES responses. Otherwise the node is still alive.
If a node recieves one YES and also an overlap, it indicates it is the leader so it indicates it and sends LeaderElected messages. 

**LeaderElected**

Sends the Leader Elected Message all across the ring and kills the process by our SIGTERM.

**IterationFinished**

Sums one to the global iterator.

**Main function**

This node.py program is a self running process and need a main function to operate. In this function there is a program argument parsing step to retrieve information of the node to instantiate. These informations are: the id, the port, the neighbour id and port, and the starting method.
The main function will then, instantiate the Node object and start its serve method to start the program.

**Manager**

This is the main program of the implementation. This manager starts every node from a given topology and gather informations about the run.
Everything is setup in the main function of the program. There is first the parsing a gathering of arguments using a custom parser. The informations are stored in topologies which is a queue of lists of nodes.
Before going through all the topologies the program setup a signal handling of the SIGNUM signal to be notified at the end of every topology termination.
It then, go through all topologies, for each one it start the nodes using the subprocess utility of python. This method allows to pass arguments to the subprocess created. The program will then, wait for termination of the current topology and then start another one while the queue is not empty.

**Utilities**

The utilities class for this program is the parser class and the yet still not implemented log class.

**Parser**

This parser allows us to parse topologies from a string or from a entire file. The simple_topology method parse a topology from a formatted string and add it to the queue. The read_file method read a file of topologies, parse them line per line and store them to the queue using the simple_topology method.
All the topologies can then, be used through the queue.

**Log.txt (developing)**
For the moment until the program works, I am logging the messages by making the program write in a node.txt file if some conditions are met so I can check it.