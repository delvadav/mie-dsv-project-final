import argparse
import signal
import subprocess
import time
import topology_parser as tp

global PORT_DECAL
PORT_DECAL = 50280
global LIMIT
LIMIT = 100
global NEXT
NEXT = False

class Node:
    def __init__(self, id, neighbours):
        self.id = id
        self.neighbours = neighbours

def start_node(node):
    args = ['python3', 'node.py', '-i', str(node.id), '-p', str(node.id + PORT_DECAL)]

    args.append('-n')
    args.append(str(node.neighbours[0]))
    args.append(str(node.neighbours[0] + PORT_DECAL))
    args.append(str(node.neighbours[1]))
    args.append(str(node.neighbours[1] + PORT_DECAL))

    subprocess.Popen(args, stdout = subprocess.DEVNULL)

def round_end(signum, frame):
    global NEXT
    NEXT = True
    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Node manager for MIE-DSV project of Recursive Doubling Leader Election implementation.')
    parser.add_argument("-t", "--topology", type=str, help="Topology of the ring of distributed node.")
    parser.add_argument("-f", "--file", type=str, help="Load topologies from a file.")

    args = parser.parse_args()

    if (args.topology and args.file) or (not args.topology and not args.file):
        parser.error(
            "\nYou need to choose exactly one option: \n\t-t for single topology \n\t-f for file with topologies")

    t_parser = tp.Parser()

    if args.topology:
        t_parser.simple_topology(args.topology)
    elif args.file:
        t_parser.read_file(args.file, LIMIT)

    c = 1
    while not (t_parser.topologies.empty()):
        print("Loading topology: " + str(c))
        nodes = t_parser.topologies.get()
        print("Topology size: " + str(len(nodes)) + " nodes")
        start = time.time()
        for n in nodes:
            start_node(n)
        while not NEXT:
            print('. ', end="", flush=True)
            time.sleep(0.5)
            print('. ', end="", flush=True)
            time.sleep(0.5)
            print('. ', end="", flush=True)
            time.sleep(0.5)

            print('\r      ', end="", flush=True)
            print('\r', end="", flush=True)
            time.sleep(0.5)
        end = time.time()
        print("Topology executed in: %.2f" % (end - start) + "s\n")
        c = c + 1
        NEXT = False
