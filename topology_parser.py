from queue import Queue
from manager import Node


class Parser:
    def __init__(self):
        self.topologies = Queue()

    def simple_topology(self, topology):
        t = topology.split(',')
        nodes = []
        n_node = len(t)
        for i in range(n_node):
            nodes.append(Node(int(t[i]), [int(t[(i - 1) % n_node]), int(t[(i + 1) % n_node])]))
        self.topologies.put(nodes)
        return nodes

    def read_file(self, file, limit):
        with open(file, 'r') as f:
            lines = f.readlines()
            for i in range(min(limit, len(lines))):
                l = lines[i].replace('\n', '')
                self.simple_topology(l)
        return self.topologies

